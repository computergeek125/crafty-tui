import datetime
import logging
import threading
import time

class CRTUI_Subscriber:
    def __init__(self, crafty_client, poll_rate=5, callback=None):
        self.logger = logging.getLogger(__name__)
        self.logger.info("Initializing the subscriber")
        self.crafty_client = crafty_client
        self.poll_rate=poll_rate
        self.callback = callback
        self.data = {'last_poll': 0}
        self.subscriber_thread_wait = threading.Event()
        self.subscriber_thread_stop = threading.Event()
        self.subscriber_thread = None
        self.reset_subscriptions()

    def reset_subscriptions(self):
        self.subscriptions = {}
        self.subscriptions['host_stats'] = False
        self.subscriptions['server_stats'] = False

    def update_subscription(self, subscription, state):
        if type(state) is not bool:
            raise ValueError("Subscription state must be a Boolean value (found {}: {}".format(subscrption, state))
        if subscription not in self.subscriptions.keys():
            raise ValueError("Invalid subscription name: {}".format(subscription))
        self.subscriptions[subscription] = state

    def get_poll_rate(self):
        return self.poll_rate

    def update_poll_rate(self, new_poll_rate):
        self.poll_rate = new_poll_rate
        if (datetime.datetime.now()-self.last_poll) >= self.poll_rate:
            self.subscriber_thread_wait.set()

    def get_callback(self):
        return self.callback

    def set_callback(self, callback):
        self.callback = callback

    def get_data(self):
        #self.logger.debug("Returning data: {}".format(self.data))
        return self.data

    def update_data(self):
        now = datetime.datetime.now()
        self.logger.debug("Fetching new data with subscriptions {}".format(self.subscriptions))
        new_data = {'last_poll': now}
        if self.subscriptions['host_stats']:
            new_data['host_stats'] = self.crafty_client.get_host_stats()
        if self.subscriptions['server_stats']:
            server_stats_temp = self.crafty_client.get_server_stats()
            server_stats_dict = {}
            for s in server_stats_temp:
                server_stats_dict[s['server_id']] = s
            new_data['server_stats'] = []
            for k in sorted(server_stats_dict.keys()):
                new_data['server_stats'].append(server_stats_dict[k])
        self.data = new_data
        if self.callback is not None:
            self.callback()

    def subscriber_loop(self):
        while(not self.subscriber_thread_stop.is_set()):
            try:
                self.update_data()
            except:
                self.logger.critical("Exception caught in subscriber loop", exc_info=True)
            self.subscriber_thread_wait.wait(timeout=self.poll_rate)
            self.subscriber_thread_wait.clear()
    
    def sub_start(self):
        self.sub_stop()
        self.subscriber_thread_stop.clear()
        self.subscriber_thread_wait.clear()
        self.subscriber_thread = threading.Thread(target=self.subscriber_loop)
        self.subscriber_thread.start()

    
    def sub_stop(self):
        self.subscriber_thread_stop.set()
        self.subscriber_thread_wait.set()
        if self.subscriber_thread is not None:
            self.subscriber_thread.join()
            self.subscriber_thread = None