#!/usr/bin/env python3

# Crafty Terminal User Interface
# By computergeek125 (macgeek125)

from crtui_subscriber import CRTUI_Subscriber

from crafty_client import CraftyWeb
from crafty_client.static import exceptions as crafty_exc
import curses
import datetime
import jsmin
import json
import logging
import pprint
import sys
import tabulate
import time

if len(sys.argv) > 1:
    config_filename = sys.argv[1]
else:
    config_filename = 'config.json'

with open(config_filename) as config_file:
    config = json.loads(jsmin.jsmin(config_file.read()))

log_format = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
logging.basicConfig(filename="crtui.log", format=log_format, level=logging.INFO)
logger = logging.getLogger(__name__)
if config['debug']:
    logger.setLevel(logging.DEBUG)
else:
    logger.setLevel(loggin.INFO)
logger.info("Program started at {d} with a log level of {l}".format(
    d=datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
    l=logger.getEffectiveLevel()
    ))

cweb = CraftyWeb(config['crafty_url'], config['crafty_token'], verify_ssl=config['verify_ssl'])
crtui_sub = CRTUI_Subscriber(cweb, poll_rate=config['bg_update_frequency'])

windows = {}
tabs = []
tab_select = 0
window_select = 0
scroll_offset = 0

def draw_header():
    win = windows['head']
    rows, cols = win.getmaxyx()
    win.border()
    win.addstr(0,0, 'Crafty Controller', curses.A_REVERSE|curses.A_BOLD|curses.A_UNDERLINE)
    win.noutrefresh()

def update_header():
    win = windows['head']
    rows, cols = win.getmaxyx()
    data = crtui_sub.get_data()
    if 'host_stats' in data:
        stats = data['host_stats']
        stat_line = ' CPU: {cpu: >6}%  RAM: {mu}/{mt}  Disk: {du}/{dt}'.format(
            cpu=stats['cpu_usage'],
            mu=stats['mem_usage'],
            mt=stats['mem_total'],
            du=stats['disk_usage'],
            dt=stats['disk_total'])
    else:
        stat_line = ' CPU: ---.--%  RAM: --.-GB/--.-GB  Disk: --.-GB/--.-GB'
    win.addnstr(1, 1, '{s: <{w}}'.format(w=cols-2, s=stat_line[:cols-2]), cols-2)
    if 'last_poll' not in data.keys() or data['last_poll'] == 0:
        date_line = "Updated: never"
    else:
        date_line = data['last_poll'].strftime('%Y-%m-%d %H:%M:%S')
    win.addstr(0,cols-len(date_line), date_line, curses.A_REVERSE)
    win.noutrefresh()

def draw_sidebar():
    win = windows['side']
    rows, cols = win.getmaxyx()
    win.border()
    win.noutrefresh()

def update_sidebar():
    win = windows['side']
    rows, cols = win.getmaxyx()
    for i in range(len(tabs)):
        if window_select == 0:
            attr_selected = curses.A_REVERSE
        else:
            attr_selected = 0
        row = '{t: <{w}}'.format(t=tabs[i][:cols-4], w=cols-4)
        if tab_select == i:
            row = '[{}]'.format(row)
            attr = attr_selected
        else:
            row = ' {} '.format(row)
            attr = 0
        win.addnstr(i+1, 1, row, cols-2, attr)
    win.noutrefresh()

def draw_main():
    win = windows['main']
    rows, cols = win.getmaxyx()
    win.clear()
    win.border()
    win.noutrefresh()

def update_main():
    win = windows['main']
    rows, cols = win.getmaxyx()
    rows_usable = rows-2
    data = crtui_sub.get_data()
    win.clear()
    win.border()
    if tab_select > 0:
        server_stats = data['server_stats'][tab_select-1]
        keys = list(server_stats.keys())
        if len(keys) > rows_usable:
            maxlen = rows_usable
        else:
            maxlen = len(keys)
        for l in range(maxlen):
            key = keys[l+scroll_offset]
            line = "{}: {}".format(key, server_stats[key])
            win.addnstr(l+1, 1,'{s: <{w}}'.format(s=line,w=cols-2),cols-2)
    win.noutrefresh()


def win_redraw():
    logger.debug("Redrawing base screen")
    draw_header()
    draw_sidebar()
    draw_main()
    curses.doupdate()

def win_update():
    logger.debug("Updating screen with new data")
    update_header()
    update_sidebar()
    update_main()
    curses.doupdate()

def data_callback():
    global tabs
    logger.debug("Received callback from subscription")
    try:
        data = crtui_sub.get_data()
        if 'server_stats' in data:
            server_stats = data['server_stats']
            tabs_new = ['Overview']
        else:
            server_stats = ["Overview"]
        for s in server_stats:
            tabs_new.append(s['name'])
        tabs = tabs_new
        win_update()
    except:
        logger.critical("Exception caught during callback", exc_info=True)

def init(stdscr):
    crtui_sub.set_callback(data_callback)
    crtui_sub.update_subscription('host_stats', True)
    crtui_sub.update_subscription('server_stats', True)
    crtui_sub.sub_start()
    
    stdscr.clear()
    windows['stdscr'] = stdscr
    windows['head'] = curses.newwin(
        config['window_parms']['header_height'],
        config['window_parms']['width'],
        config['window_parms']['offset_y'],
        config['window_parms']['offset_x'])
    windows['side'] = curses.newwin(
        config['window_parms']['height']-config['window_parms']['header_height']+1,
        config['window_parms']['sidebar_width'],
        windows['head'].getmaxyx()[0]+windows['head'].getbegyx()[0]-1,
        config['window_parms']['offset_x'])
    windows['main'] = curses.newwin(
        config['window_parms']['height']-config['window_parms']['header_height']+1,
        config['window_parms']['width']-config['window_parms']['sidebar_width']+1, 
        windows['head'].getmaxyx()[0]+windows['head'].getbegyx()[0]-1,
        windows['side'].getmaxyx()[1]+windows['side'].getbegyx()[1]-1)

def main(stdscr):
    global window_select
    global tab_select
    init(stdscr)
    win = windows['main']
    data = crtui_sub.get_data()
    win_redraw()
    win_update()
    while(True):
        try:
            key = win.getkey()
            if key == '\x1b':
                k1 = win.getkey()
                k2 = win.getkey()
                key = key+k1+k2
            if window_select == 0:
                if key == '\t':
                    window_select = 1
                elif key == '\x1b[A':
                    if tab_select > 0:
                        tab_select -= 1
                elif key == '\x1b[B':
                    if tab_select < len(tabs):
                        tab_select += 1
                    if tab_select >= len(tabs):
                        tab_select = len(tabs-1)
            elif window_select == 1:
                if key == '\t':
                    window_select = 0
                elif key == '\x1b[A':
                    scroll_offset -= 1
                elif key == '\x1b[B':
                    scroll_offset += 1
            if config['debug']:
                logger.debug(key)
                kes = repr(key)
                win.addnstr(win.getmaxyx()[0]-1, win.getmaxyx()[1]-len(kes)-1,
                    kes,len(kes),curses.A_REVERSE)
            win_update()
        except KeyboardInterrupt:
            break
        except:
            logger.critical("Exception caught in main handler", exc_info=True)
try:
    curses.wrapper(main)
finally:
    if crtui_sub is not None:
        crtui_sub.sub_stop()