from crtui_subscriber import CRTUI_Subscriber

from crafty_client import CraftyWeb
from crafty_client.static import exceptions as crafty_exc
import datetime
import json
import jsmin
import logging
import pprint
import sys
import time

def printer():
    pprint.pprint(crtui_sub.get_data())

log_format = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
logging.basicConfig(filename="sub_test.log", format=log_format, level=logging.DEBUG)
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
logger.info("Program started at {d} with a log level of {l}".format(
    d=datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
    l=logger.getEffectiveLevel()
    ))

if len(sys.argv) > 1:
    config_filename = sys.argv[1]
else:
    config_filename = 'config.json'

with open(config_filename) as config_file:
    config = json.loads(jsmin.jsmin(config_file.read()))

cweb = CraftyWeb(config['crafty_url'], config['crafty_token'], verify_ssl=config['verify_ssl'])

crtui_sub = CRTUI_Subscriber(cweb, poll_rate=1, callback=printer)
crtui_sub.sub_start()
crtui_sub.update_subscription('host_stats', True)
crtui_sub.update_subscription('server_stats', True)

while(True):
    try:
        time.sleep(10)
    except KeyboardInterrupt:
        break;

crtui_sub.sub_stop()